# ProjetL
A UE 5 game made during courses of Master in Computers Sciences at University of Quebec at Chicoutimi, at the winter 2023 trimestral. 

## Summary 
ProjetL is a single-player survival game where the aim is to escape from a desert island. You'll need to find the various items you'll need to craft equipment and increase your chances of survival. But beware, there's a monster lurking nearby. Good luck!

## Core feature

- Craft and inventory system using datatable
- Enemy ai 
- Quest system 

## Credits

Developpers    

- Marc-Antoine Jean   
- Maxime Cosialls
- Kilian Bauvent
- Dimitri Varré
