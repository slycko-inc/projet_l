﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Items/ItemBase.h"
#include "Resource.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnResourceDead);

UCLASS()
class PROJET_L_API AResource : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AResource();
	
	UPROPERTY(VisibleDefaultsOnly, Category=Collision)
	UBoxComponent* CollisionComp;

	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	UStaticMeshComponent* Mesh;
	
	UPROPERTY(EditAnywhere, Category=Item)
	TSubclassOf<AActor> Item2Drop;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Tool)
	TSubclassOf<AActor> ToolToBreak;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category=Health)
	int MaxHealth;

	UPROPERTY(BlueprintAssignable)
	FOnResourceDead OnResourceDead;

	UFUNCTION(BlueprintCallable, Category=Health)
	void OnTakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(BlueprintReadOnly, Category=Health)
	int Health;

};
