// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/DataTable.h"
#include "Items/ItemBase.h"
#include "InventoryComponent.generated.h"

USTRUCT(BlueprintType)
struct FItemSlot
{
	GENERATED_BODY()
		
	UPROPERTY(BlueprintReadWrite)
	FItemBaseStruct Item;
	
	UPROPERTY(BlueprintReadWrite)
	int Quantity;

	//to use contains :
	bool operator==(const FItemSlot& ItemSlot) const;
	
	// static bool operator==(const FItemSlot &itemSlot1, const FItemSlot &itemSlot2) const;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInventoryUpdate);

//TODO: Add the class component to the character BP as done here : https://youtu.be/vHT4MhmwacE?list=PL4G2bSPE_8umjCYXbq0v5IoV-Wi_WAxC3&t=319
UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class PROJET_L_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent();

	UFUNCTION(BlueprintCallable,BlueprintPure)
	TArray<FItemSlot> GetInventory(){return Inventory;};

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	int InventorySize;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TArray<FItemSlot> Inventory;

	UPROPERTY()
	bool bIsEmpty;

	//virtual void AddToInventory(AItemBase * Item);
	UFUNCTION(BlueprintCallable,Category="Inventory")
	void AddToInventory(FItemBaseStruct Item, int Nb);

	UFUNCTION(BlueprintCallable, Category="Inventory")
	void AddToInventoryX2(FItemBaseStruct Item, int Nb);
	
	
	UFUNCTION(BlueprintCallable)
	virtual void RemoveFromInventory(int itemIndex, int Nb, FItemBaseStruct remover);
	
	UFUNCTION(BlueprintCallable)
	virtual void TransferSlot(const int SourceIndex, const int DestinationIndex);

	UFUNCTION(BlueprintCallable)
	void TransferItem(const int SourceIndex, const int DestinationIndex, UInventoryComponent* sourceInventory, FItemSlot transferedItem, FItemBaseStruct remover);

	UFUNCTION(BlueprintCallable)
	TArray<FItemSlot> InitInventory(FItemSlot ItemSlot, int Cpt);

	UPROPERTY(BlueprintAssignable)
	FOnInventoryUpdate OnInventoryUpdate;
	
	UFUNCTION(BlueprintCallable)
	void UseObject(int itemIndex);
	
	UFUNCTION(BlueprintCallable)
	int indexOfItem(FName itemName);
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};


