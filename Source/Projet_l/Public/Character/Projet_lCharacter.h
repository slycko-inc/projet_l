// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "InputActionValue.h"
#include "NativeGameplayTags.h"
#include "TaggedInputConfig.h"
#include "TaggedKeyboardContext.h"
#include "Inventory/InventoryComponent.h"
#include "Projet_lCharacter.generated.h"

class UInputComponent;
class USkeletalMeshComponent;
class USceneComponent;
class UCameraComponent;
class UAnimMontage;
class USoundBase;

UE_DECLARE_GAMEPLAY_TAG_EXTERN(TAG_Input_Attack);
UE_DECLARE_GAMEPLAY_TAG_EXTERN(TAG_Input_Interact);
UE_DECLARE_GAMEPLAY_TAG_EXTERN(TAG_Input_Jump);
UE_DECLARE_GAMEPLAY_TAG_EXTERN(TAG_Input_Look);
UE_DECLARE_GAMEPLAY_TAG_EXTERN(TAG_Input_Move);
UE_DECLARE_GAMEPLAY_TAG_EXTERN(TAG_Input_OpenInventory)
UE_DECLARE_GAMEPLAY_TAG_EXTERN(TAG_Input_ToggleSelectionWheel)

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAttack);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInteract, AItemBase*, Item);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnMove);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBellowFiftyPercentLifeDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnPickupItemDelegate, FItemBaseStruct, ItemBaseStruct, int, ItemToAdd);


UCLASS(config=Game)
class AProjet_lCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Mesh, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* Mesh1P;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;
	

	/** Input Config **/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UTaggedInputConfig * InputConfig;
	
public:
	AProjet_lCharacter();

protected:
	virtual void BeginPlay() override;

	/** Input Contexts */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category=Input)
	TArray<FTaggedKeyboardContext> InputContexts;


public:
	/** Bool for AnimBP to switch to another animation set */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Weapon)
	bool bHasRifle;

	/** Setter to set the bool */
	UFUNCTION(BlueprintCallable, Category = Weapon)
	void SetHasRifle(bool bNewHasRifle);

	/** Getter for the bool */
	UFUNCTION(BlueprintCallable, Category = Weapon)
	bool GetHasRifle();

	UPROPERTY(BlueprintAssignable)
	FOnAttack OnAttack;

	UPROPERTY(BlueprintAssignable)
	FOnInteract OnInteract;

	UPROPERTY(BlueprintAssignable)
	FOnDeath OnDeath;

	UPROPERTY(BlueprintAssignable)
	FOnDeath OnMove;

	UPROPERTY(BlueprintAssignable)
	FOnPickupItemDelegate OnPickupItem;
	
	UPROPERTY(BlueprintAssignable)
	FOnBellowFiftyPercentLifeDelegate OnBellowFiftyPercentLife;

protected:
	/** Called for movement input */
	void Move(const FInputActionValue& Value);

	/** Called for looking input */
	void Look(const FInputActionValue& Value);
	
	/** Called for open inventory input */
	UFUNCTION(BlueprintCallable)
	void OpenInventory(const FInputActionValue& Value);

	/** Called for looking input */
	void Interact();

	/** Called for Attack input */
	void Attack();

	/** Called for Attack input */
	void ToggleSelectionWheel();
	
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

	//status :
	


public:

	UPROPERTY(BlueprintReadWrite,EditAnywhere, Category="PlayerStats")
	int playerHealth;
	
	UPROPERTY(BlueprintReadWrite,EditAnywhere, Category="PlayerStats")
	int maxPlayerHealth = 100;

	UPROPERTY(BlueprintReadWrite,EditAnywhere, Category="PlayerStats")
	bool isDead = false;
	
	/** Returns Mesh1P subobject **/
	USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

	/*UFUNCTION(BlueprintCallable, Category="Player Health")
	int PlayerHealth() const{return playerHealth;}
	UFUNCTION(BlueprintCallable, Category="Player Health")
	int MaxPlayerHealth() const{return maxPlayerHealth;};
	UFUNCTION(BlueprintCallable, Category="Player Health")
	int IsDead() const{return this->isDead;};
	*/
	UFUNCTION(BlueprintCallable, Category="Player Health")
	void HealPlayer(int HealAmount);
	UFUNCTION(BlueprintCallable, Category="Player Health")
	void LoseLife(int LoseAmount);
};

