// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Projet_lGameMode.generated.h"

UCLASS(minimalapi)
class AProjet_lGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AProjet_lGameMode();
};



