// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "PLHUD.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class PROJET_L_API APLHUD final : public AHUD
{
	GENERATED_BODY()
public: 
	// APLHUD();
	APLHUD(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;
	
	UFUNCTION(BlueprintCallable)
	void ShowInventory();
	UFUNCTION(BlueprintCallable)
	void HideInventory();

	UFUNCTION(BlueprintCallable)
	void ShowSelectionWheel();

	bool inventoryIsOpen;

	UFUNCTION(BlueprintCallable)
	void HideHUD();
	
	UFUNCTION(BlueprintCallable)
	void DisplayHUD();
	

protected:
	UPROPERTY(EditDefaultsOnly, Category="Inventory")
		TSubclassOf<class UUserWidget> InventoryClass;

	UPROPERTY()
		class  UUserWidget* Inventory;

	UPROPERTY()
	UUserWidget* WeaponWheel;
	
	UPROPERTY(EditDefaultsOnly, Category="Weapons")
	TSubclassOf<class UUserWidget> WeaponWheelClass;

	UPROPERTY(EditDefaultsOnly, Category="Player Health")
	TSubclassOf<class  UUserWidget> playerHealthBarClass;
	UPROPERTY()
	class  UPlayerHealthBar* playerHealthBar;
};
