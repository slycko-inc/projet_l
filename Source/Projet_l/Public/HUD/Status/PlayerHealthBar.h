// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Character/Projet_lCharacter.h"
#include "PlayerHealthBar.generated.h"

/**
 * 
 */
UCLASS(Abstract, Category="Player Health")
class PROJET_L_API UPlayerHealthBar : public UUserWidget
{
	GENERATED_BODY()
public:
	void SetOwnerPlayerCharacter(AProjet_lCharacter* playerCharacter) {this->OwnerPlayerCharacter = playerCharacter; }
	
protected:
	void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	void setHealthBarPercent();
	void setHealthTextBoxes();

	TWeakObjectPtr<AProjet_lCharacter> OwnerPlayerCharacter;
	
	UPROPERTY(meta=(BindWidget))
	class UProgressBar* healthBar;
	UPROPERTY(meta=(BindWidget))
	class UTextBlock* currentHealth;
	UPROPERTY(meta=(BindWidget))
	class UTextBlock* MaxHealth;
};
