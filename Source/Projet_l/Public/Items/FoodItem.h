// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ItemBase.h"
#include "FoodItem.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class PROJET_L_API AFoodItem : public AItemBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="item")
	float HungerToRecover;
};
