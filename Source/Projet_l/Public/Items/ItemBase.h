﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NativeGameplayTags.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "Items/ItemData.h"
#include "ItemBase.generated.h"

USTRUCT(BlueprintType)
struct FItemBaseStruct : public FTableRowBase
{
	GENERATED_BODY()
	
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item")
	FName RowName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item")
	int Id;*/

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item")
	FName ItemName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item")
	FText DisplayName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item")
	FText Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item")
	int MaxStack;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item")
	class UTexture2D* Thumbnail;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item")
	TArray<FGameplayTag> tags;      
};

UCLASS(BlueprintType, Blueprintable)
class PROJET_L_API AItemBase : public AActor
{
	GENERATED_BODY()

public :
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category=Item)
	UBoxComponent* CollisionComp;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category=Collision)
	UStaticMeshComponent* Mesh;
	

	// Sets default values for this actor's properties
	AItemBase();

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item")
	//FDataTableRowHandle ItemData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Item")
	FItemBaseStruct ItemInfo;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	
};
