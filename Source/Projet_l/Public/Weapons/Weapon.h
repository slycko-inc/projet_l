﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Sockets.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Weapon.generated.h"


UCLASS(BlueprintType, Blueprintable)
class PROJET_L_API AWeapon : public AActor
{
	GENERATED_BODY()
public:
	// Sets default values for this actor's properties
	AWeapon();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Weapon)	
	UStaticMeshComponent * Mesh;
};
