// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/InventoryComponent.h"


#include "UtilityHelpers/UtilityHelpers.h"

bool FItemSlot::operator==(const FItemSlot& ItemSlot) const
{
	return Item.ItemName.IsEqual(ItemSlot.Item.ItemName);
}

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	this->InventorySize = 25;
	bIsEmpty = true;
}

/*void UInventorySystem::AddToInventory(AItemBase * Item)
{
	const FString ContextString = "Searching for item";
	AddToInventory(Item->ItemData.RowName, 1);
}*/

void UInventoryComponent::AddToInventory(FItemBaseStruct Item, int Nb)
{
	// UtilityHelpers::Log(FString("item searched = ").Append(itemID.ToString()));

	//check if the item is already in the inventory :
	
	FItemSlot tempItemSlot = FItemSlot{Item, 0};
	if(this->Inventory.Contains(FItemSlot{Item, 0}))
	{
		const int i = this->Inventory.Find(FItemSlot{Item, 0});
		this->Inventory[i].Quantity += Nb; //TODO: check if works 
	}
	else
	{
		//replace the first empty slot found :
		/*for(auto& [_item, Quantity] : this->Inventory)
		{
			if(Item.ItemName.IsEqual("Empty"))
			{
				_item = Item;
				Quantity = nb;
				break;
			}
		}*/

		for (auto& ItemSlot : Inventory)
		{
			if(ItemSlot.Item.ItemName.IsEqual("Empty"))
			{
				ItemSlot.Item.ItemName = Item.ItemName;
				ItemSlot.Item.Description = Item.Description;
				ItemSlot.Item.MaxStack = Item.MaxStack;
				ItemSlot.Item.Thumbnail = Item.Thumbnail;

				ItemSlot.Quantity = Nb;
				break;
			}
		}
	}
	
	OnInventoryUpdate.Broadcast();
}

void UInventoryComponent::AddToInventoryX2(FItemBaseStruct Item, int Nb)
{
	for (auto& ItemSlot : Inventory)
	{
		if(ItemSlot.Item.ItemName.IsEqual("Empty"))
		{
			ItemSlot.Item.ItemName = Item.ItemName;
			ItemSlot.Item.Description = Item.Description;
			ItemSlot.Item.MaxStack = Item.MaxStack;
			ItemSlot.Item.Thumbnail = Item.Thumbnail;

			ItemSlot.Quantity = Nb;
			break;
		}
	}

	OnInventoryUpdate.Broadcast();
}

void UInventoryComponent::RemoveFromInventory(int itemIndex, int Nb, FItemBaseStruct remover)
{

	this->Inventory[itemIndex].Quantity -= Nb;
	if(this->Inventory[itemIndex].Quantity == 0)
	{
		this->Inventory[itemIndex].Item = remover;
	}
	OnInventoryUpdate.Broadcast();
}

void UInventoryComponent::TransferSlot(const int SourceIndex, const int DestinationIndex)
{
	//TODO delete me
	const auto& Source = this->Inventory[SourceIndex];
	const auto& Destination = this->Inventory[DestinationIndex];
	const auto Temp = Destination;

	this->Inventory[DestinationIndex] = Source;
	this->Inventory[SourceIndex] = Temp;
	int i =0;

	//call the inventory update :
	OnInventoryUpdate.Broadcast();
}

void UInventoryComponent::TransferItem(const int SourceIndex, const int DestinationIndex,
	UInventoryComponent* sourceInventory, FItemSlot transferedItem, FItemBaseStruct remover)
{
	this->AddToInventory(transferedItem.Item, transferedItem.Quantity);
	sourceInventory->RemoveFromInventory(SourceIndex, transferedItem.Quantity, remover);
}


TArray<FItemSlot> UInventoryComponent::InitInventory(FItemSlot ItemSlot, int Cpt)
{
	TArray<FItemSlot> _temp;
	while (Cpt!=InventorySize)
	{
		_temp.Add(ItemSlot);
		Cpt++;
	}

	return _temp;
}

void UInventoryComponent::UseObject(int itemIndex)
{
	FItemBaseStruct itemToUse = this->Inventory[itemIndex].Item;
	//TODO: Add a tag array on FItemBaseStruct to distinguish food, drink and ressources
	// if(itemToUse.Tags.Contains(FName("Food")))
	// 	this->EatFood(itemIndex, itemToUse);
	// if(itemToUse.Tags.Contains(FName("Beverage")))
	// 	this->DrinkBeverage(itemIndex, itemToUse);
}

int UInventoryComponent::indexOfItem(FName itemName)
{
	for(int i = 0; i < this->Inventory.Num(); i++)
		if(this->Inventory[i].Item.ItemName.IsEqual(itemName))
			return i;

	return -1;
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	FItemBaseStruct _item = FItemBaseStruct();
	_item.ItemName = "Empty";
	//initialise the inventory
	for(int i =0; i < this->InventorySize; i++)
	{
		this->Inventory.Add(FItemSlot{_item, 0});
	}

}


// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


