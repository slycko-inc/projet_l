﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Resources/Resource.h"


// Sets default values
AResource::AResource()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CollisionComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));

	RootComponent = CollisionComp;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));

	Mesh->SetupAttachment(RootComponent);
}


// Called when the game starts or when spawned
void AResource::BeginPlay()
{
	Super::BeginPlay();

	Health = MaxHealth;

	OnTakeAnyDamage.AddDynamic(this, &AResource::OnTakeDamage);
}

void AResource::OnTakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser)
{
	UE_LOG(LogTemp, Warning, TEXT("Resource take damage"));
	Health -= Damage;

	if(Health <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Resource is dead"));

		OnResourceDead.Broadcast();

		const auto ResourceLocation = GetActorLocation();
		GetWorld()->SpawnActor(Item2Drop, &ResourceLocation);
		
		Destroy();
	}
}

