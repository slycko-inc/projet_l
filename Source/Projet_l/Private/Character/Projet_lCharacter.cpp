// Copyright Epic Games, Inc. All Rights Reserved.


#include "Character/Projet_lCharacter.h"

#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "TaggedInputComponent.h"
#include "Blueprint/UserWidget.h"
#include "HUD/PLHUD.h"
#include "Items/ItemBase.h"
#include "Kismet/GameplayStatics.h"

UE_DEFINE_GAMEPLAY_TAG(TAG_Input_Attack, "Input.Attack")
UE_DEFINE_GAMEPLAY_TAG(TAG_Input_Interact, "Input.Interact")
UE_DEFINE_GAMEPLAY_TAG(TAG_Input_Jump, "Input.Jump")
UE_DEFINE_GAMEPLAY_TAG(TAG_Input_Look, "Input.Look")
UE_DEFINE_GAMEPLAY_TAG(TAG_Input_Move, "Input.Move")
UE_DEFINE_GAMEPLAY_TAG(TAG_Input_OpenInventory, "Input.OpenInventory")
UE_DEFINE_GAMEPLAY_TAG(TAG_Input_ToggleSelectionWheel, "Input.ToggleSelectionWheel")
UE_DEFINE_GAMEPLAY_TAG(TAG_ItemTag_Pickable, "ItemTag.Pickable")
UE_DEFINE_GAMEPLAY_TAG(TAG_ItemTag_Container, "ItemTag.Container")


//////////////////////////////////////////////////////////////////////////
// AProjet_lCharacter

AProjet_lCharacter::AProjet_lCharacter()
{
	// Character doesnt have a rifle at start
	bHasRifle = false;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-10.f, 0.f, 60.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	//Mesh1P->SetRelativeRotation(FRotator(0.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-30.f, 0.f, -150.f));
}

void AProjet_lCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//status:
	this->playerHealth = this->maxPlayerHealth;

	const APlayerController* PlayerController = Cast<APlayerController>(Controller);
	if (!PlayerController) return;

	const auto* LocalPlayer = PlayerController->GetLocalPlayer();
	if (!LocalPlayer) return;

	auto* EnhancedInputSubsystem = LocalPlayer->GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(LocalPlayer);
	if (!ensureMsgf(EnhancedInputSubsystem,
	                TEXT("A Local player was detected but it is not configured to use Enhanced Input"))) return;

	const auto Layout = GetGameInstance()->GetSubsystem<UTaggedInputSubsystem>()->CheckKeyboardLayout();

	for (const auto& [InputLayout, MappingContext] : InputContexts)
	{
		if (MappingContext && InputLayout == Layout)
		{
			EnhancedInputSubsystem->AddMappingContext(MappingContext, 0);
		}
	}
}

//////////////////////////////////////////////////////////////////////////// Input

void AProjet_lCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	// Set up action bindings
	auto* TaggedInputComponent = CastChecked<UTaggedInputComponent>(PlayerInputComponent);

	//Jumping
	TaggedInputComponent->BindActionByTag(InputConfig, TAG_Input_Jump, ETriggerEvent::Triggered, this,
	                                      &ACharacter::Jump);
	TaggedInputComponent->BindActionByTag(InputConfig, TAG_Input_Jump, ETriggerEvent::Completed, this,
	                                      &ACharacter::StopJumping);
	//Moving
	TaggedInputComponent->BindActionByTag(InputConfig, TAG_Input_Move, ETriggerEvent::Triggered, this,
	                                      &AProjet_lCharacter::Move);

	//Looking
	TaggedInputComponent->BindActionByTag(InputConfig, TAG_Input_Look, ETriggerEvent::Triggered, this,
	                                      &AProjet_lCharacter::Look);
	//Open the inventory:
	TaggedInputComponent->BindActionByTag(InputConfig, TAG_Input_OpenInventory, ETriggerEvent::Completed, this,
	                                      &AProjet_lCharacter::OpenInventory);
	//Interact
	TaggedInputComponent->BindActionByTag(InputConfig, TAG_Input_Interact, ETriggerEvent::Completed, this,
										  &AProjet_lCharacter::Interact);

	//Attack
	TaggedInputComponent->BindActionByTag(InputConfig, TAG_Input_Attack, ETriggerEvent::Completed, this,
										  &AProjet_lCharacter::Attack);

	//Toggle Selection Wheel
	TaggedInputComponent->BindActionByTag(InputConfig, TAG_Input_ToggleSelectionWheel, ETriggerEvent::Completed, this,
										  &AProjet_lCharacter::ToggleSelectionWheel);

}

void AProjet_lCharacter::HealPlayer(int HealAmount)
{
	this->playerHealth += HealAmount;
	if(this->playerHealth > this->maxPlayerHealth)
		this->playerHealth = this->maxPlayerHealth;
}

void AProjet_lCharacter::LoseLife(int LoseAmount)
{
	this->playerHealth -= LoseAmount;

	if(playerHealth < (maxPlayerHealth/2))
		OnBellowFiftyPercentLife.Broadcast();
	
	if(this->playerHealth <= 0){
		OnDeath.Broadcast();
	}
}


void AProjet_lCharacter::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	const FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add movement 
		AddMovementInput(GetActorForwardVector(), MovementVector.Y);
		AddMovementInput(GetActorRightVector(), MovementVector.X);
	}

	OnMove.Broadcast();
}

void AProjet_lCharacter::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	const FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void AProjet_lCharacter::OpenInventory(const FInputActionValue& Value)
{
	//Create player menu widget :
	APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	APLHUD* HUD = PC->GetHUD<APLHUD>();
	HUD->ShowInventory();
}

void AProjet_lCharacter::Interact()
{
	UE_LOG(LogTemp, Warning, TEXT("Interact"));
	
	TArray<AActor*> OverlappingActors;
	GetOverlappingActors(OverlappingActors);

	for (const auto OverlappingActor : OverlappingActors)
	{
		const auto Item = Cast<AItemBase>(OverlappingActor);
		if(Item){
			if(Item->ItemInfo.tags.Contains(TAG_ItemTag_Pickable))
			{
			
				// TODO: check if item has gameplay tag pickable

				auto Comps = GetComponentsByClass(UInventoryComponent::StaticClass());
				const auto Inventory = Cast<UInventoryComponent>(Comps[0]);
				Inventory->AddToInventory(Item->ItemInfo,1);
				//InventoryComponent->AddToInventory(Item->ItemInfo,1);

				OnPickupItem.Broadcast(Item->ItemInfo, 1);
			
				UE_LOG(LogTemp, Warning, TEXT("Pick up item"));

				Item->Destroy();

				return;
			}
			else
			{
				OnInteract.Broadcast(Item);
			}
		}
	}
}

void AProjet_lCharacter::Attack()
{
	OnAttack.Broadcast();
	UE_LOG(LogTemp, Warning, TEXT("Attack"));
}

void AProjet_lCharacter::ToggleSelectionWheel()
{
	//Create player menu widget :
	const APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	APLHUD* HUD = PC->GetHUD<APLHUD>();
	HUD->ShowSelectionWheel();
}


void AProjet_lCharacter::SetHasRifle(bool bNewHasRifle)
{
	bHasRifle = bNewHasRifle;
}

bool AProjet_lCharacter::GetHasRifle()
{
	return bHasRifle;
}
