// Fill out your copyright notice in the Description page of Project Settings.


#include "HUD/PLHUD.h"

#include <GameFramework/PlayerController.h>
#include "Blueprint/UserWidget.h"
#include <Kismet/GameplayStatics.h>

#include "HAL/Platform.h"
#include "HAL/Platform.h"
#include "HUD/Status/PlayerHealthBar.h"
#include "Misc/AssertionMacros.h"
#include "Util/ColorConstants.h"
#include "UtilityHelpers/UtilityHelpers.h"

// APLHUD::APLHUD()
// {
// 	// this->Inventory->Initialize();.
// 	this->inventoryIsOpen = false;
// }
//Super(ObjectInitializer) -> default construct 
APLHUD::APLHUD(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	this->inventoryIsOpen = false;

}

void APLHUD::BeginPlay()
{
	Super::BeginPlay();

	//get the player health bar :
	UtilityHelpers::Log("player health bar added to viewPort !", true, this);

	// this->playerHealthBar = ObjectInitializer.CreateDefaultSubobject<UUserWidget>(GetWorld(), TEXT("PlayerHealthBar"));	
	this->DisplayHUD();
	
	// UtilityHelpers::Log("player health bar added to viewPort !", true);
	
}


void APLHUD::ShowInventory()
{
	check(GetOwner() != nullptr);
	APlayerController* PC = Cast<APlayerController>(GetOwner());
	Inventory = CreateWidget<UUserWidget>(PC, InventoryClass);
	
	Inventory->AddToViewport();
	this->inventoryIsOpen = true;
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Openning inventory: %b"), (this->inventoryIsOpen? TEXT("true") : TEXT("false"))));
}


void APLHUD::HideInventory()
{
	if(Inventory)
	{
		Inventory->RemoveFromParent();
		Inventory = nullptr;
	}
	this->inventoryIsOpen = false;
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Closing inventory: %b"), (this->inventoryIsOpen? TEXT("true") : TEXT("false"))));
}

void APLHUD::ShowSelectionWheel()
{
	WeaponWheel = CreateWidget(GetWorld(), WeaponWheelClass);
	if(WeaponWheel)
	{
		WeaponWheel->AddToViewport();
	}
}

void APLHUD::HideHUD()
{
	if(this->playerHealthBar)
	{
		playerHealthBar->SetVisibility(ESlateVisibility::Hidden);
		// playerHealthBar->RemoveFromParent();
		// playerHealthBar = nullptr;
	}

	// HideInventory();
}

void APLHUD::DisplayHUD()
{
	if(this->playerHealthBar == __nullptr)
	{
		this->playerHealthBar = (UPlayerHealthBar*) CreateWidget<UUserWidget>(GetWorld(), this->playerHealthBarClass);
		
		if(this->playerHealthBar != __nullptr){
		this->playerHealthBar->AddToViewport();
		this->playerHealthBar->SetOwnerPlayerCharacter((AProjet_lCharacter*) UGameplayStatics::GetPlayerCharacter(GetWorld(),0));
		}
	}

	this->playerHealthBar->SetVisibility(ESlateVisibility::Visible);
}

