﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapons/Weapon.h"


// Sets default values
AWeapon::AWeapon()
{
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));

	Mesh->SetupAttachment(RootComponent);
}


