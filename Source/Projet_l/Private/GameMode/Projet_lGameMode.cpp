// Copyright Epic Games, Inc. All Rights Reserved.


#include "GameMode/Projet_lGameMode.h"

#include "Character/Projet_lCharacter.h"
#include "UObject/ConstructorHelpers.h"

AProjet_lGameMode::AProjet_lGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/Character/BP_Player"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;
}
