﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TaggedInputSubsystem.h"

#if PLATFORM_WINDOWS
#include "Windows/WindowsHWrapper.h"
#endif

ETaggedInputKeyboardLayout UTaggedInputSubsystem::CheckKeyboardLayout()
{
#if PLATFORM_WINDOWS
	switch (PRIMARYLANGID(LOWORD( GetKeyboardLayout( 0 ) )))
	{
	case LANG_FRENCH:
		return ETaggedInputKeyboardLayout::Azerty;
		break;
	case LANG_GERMAN:
		return ETaggedInputKeyboardLayout::Qwertz;
		break;
	default:
		return ETaggedInputKeyboardLayout::Qwerty;
		break;
	}
#else
	return ETaggedInputKeyboardLayout::Qwerty;
#endif
}
