﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TaggedInputConfig.h"

const UInputAction* UTaggedInputConfig::FindInputActionForTag(const FGameplayTag Tag) const
{
	for (const auto& [InputAction, InputTag] : TaggedInputActions)
	{
		if(InputAction && InputTag == Tag)
		{
			return InputAction;
		}
	}

	return nullptr;
}
